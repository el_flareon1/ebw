from flask import Flask, render_template, request
import numpy as np
import tensorflow as tf
import sklearn as sl
from joblib import load

app = Flask(__name__)


@app.route('/', methods=["get", "post"])
def predict():
    message = ""

    if request.method == "POST":
        IW = request.form.get("IW")
        IF = request.form.get("IF")
        VW = request.form.get("VW")
        FP = request.form.get("FP")


        element = [[float(IW), float(IF), float(VW), float(FP)]]
        X_scaler = load("X_scaler.joblib")
        Y_scaler = load("Y_scaler.joblib")
        element = X_scaler.transform(element)
        model_loaded = tf.keras.models.load_model("ebw_model")
        pred = model_loaded.predict(element)
        pred = Y_scaler.inverse_transform(pred)
        Width = '{:.2f}'.format(pred[0][0])
        Depth = '{:.2f}'.format(pred[0][1])
        message = f"Ширина сварного шва (Width): {Width} | Глубина сварного шва (Depth): {Depth}"


    return render_template("index.html", message=message)
